import { createMuiTheme } from '@material-ui/core/styles';
import { sisccTheme } from '@sis-cc/dotstatsuite-visions';
import * as R from 'ramda';

export default R.pipe(
  R.over(
    R.lensPath(['mixins', 'chart']),
    R.mergeDeepLeft({
      main: { fontFamily: 'bernino-sans-narrow-regular' },
      annotation: { fontFamily: 'bernino-sans-narrow-bold' },
      axisLegend: { fontFamily: 'bernino-sans-narrow-bold' },
      legend: { fontFamily: 'bernino-sans-narrow-bold' },
      tooltip: {
        secondary: { fontFamily: 'bernino-sans-bold' }
      }
    })
  ),
  R.assocPath(['mixins', 'dataHeader', 'title', 'fontFamily'], 'caecilia'),
  R.assocPath(['mixins', 'dataHeader', 'root', 'fontFamily'], 'bernino-sans-narrow-regular'),
  R.assocPath(['typography', 'fontFamily'], 'bernino-sans-narrow-regular'),
  createMuiTheme
)(sisccTheme({ rtl: false }));
