import React, { Component } from 'react';
import { render } from 'react-dom';
import * as R from 'ramda';
import { get, isNil, isString, reduce } from 'lodash';
import ReactGA from 'react-ga';
import { Viewer as ViewerComp } from '@sis-cc/dotstatsuite-components';
import { v7DataFormatPatch } from '@sis-cc/dotstatsuite-sdmxjs';
import { IntlProvider, defineMessages, injectIntl, addLocaleData, FormattedMessage } from 'react-intl';
import { i18nMessages } from './modules/i18n';
import { LATEST, SNAPSHOT, NO_SOURCE } from './constants';
import { appendTimelineTooltip, handleError, getQueryVariable, getDataFromJson, getLatestData, isDataEmpty, sendAnalytics } from './utils';
import { rules } from '@sis-cc/dotstatsuite-components';
import numeral from 'numeral';
import { Helmet } from 'react-helmet';
import favicon from './assets/favicon.ico';
import './assets/app.less';
import { ThemeProvider } from '@material-ui/core/styles';
import mUiTheme from './theme';
import meta from '../package.json';
console.info(`[${process.env.NODE_ENV}#${process.env.TARGET_ENV}#${process.env.APP_ENV}]::[${meta.name}@${meta.version}]`);

const Viewer = injectIntl(
  ({ intl, type, data, messages }) => {
    const headerProps = R.pick(['title', 'subtitle', 'uprs'], data.data);
    const footerProps = {
      logo: R.path(['config', 'logo'], data),
      source: R.path(['data', 'source'], data),
      copyright: R.isNil(R.path(['data', 'copyright'], data))
        ? null
        : { label: '©', content: <p>©OECD <a href="https://www.oecd.org/termsandconditions/">Terms & conditions</a></p> }
    };
    const chartData = R.pick(['series', 'frequency', 'share'], data.data);

    return (
      <div>
        <ViewerComp
          chartData={chartData}
          chartOptions={data.options}
          headerProps={headerProps}
          footerProps={footerProps}
          type={type}
        />
      </div>
    );
  }
);

class App extends Component {
  constructor(props) {
    super(props);

    const gaTrackingId = get(props.config, 'GA.trackingID');
    if (gaTrackingId) {
      this.gaTrackingId = gaTrackingId;
      ReactGA.initialize(gaTrackingId);
      ReactGA.pageview(window.location.pathname + window.location.search);
    }
    this.endpoint = get(props.config, 'chart.api.share.endpoint');
    this.state = { isFetching: false, error: null, locale: 'en', messages: i18nMessages['en'] };
  }

  componentDidMount = () => {
    if (!this.endpoint) return console.log('no endpoint');

    this.setState({ isFetching: true, error: null });
    const shareId = getQueryVariable(window.location, 'id');
    fetch(`${this.endpoint}/${shareId}`)
      .then(handleError)
      .then(sendAnalytics(this.gaTrackingId, shareId))
      .then((json) => {
        const type = json.type;
        const data = getDataFromJson(type)(json);
        const locale = get(data, 'config.locale.id', 'en');
        const delimiters = get(data, 'config.locale.delimiters');

        if (!isNil(delimiters)) {
          numeral.locale(`${locale}/${locale}`);
          numeral.register('locale', `${locale}/${locale}`, { delimiters });
        }

        if (json.share === SNAPSHOT) {
          return this.setState({
            isFetching: false,
            type,
            data,
            locale,
            messages: i18nMessages[locale],
          });
        } else if (json.share === LATEST) {
          const source = get(data, 'data.share.source');
          const headers = get(data, 'config.sourceHeaders');
          if (source && source !== NO_SOURCE) {
            fetch(source, { headers }).then(handleError)
              .then((json) => v7DataFormatPatch(json))
              .then((json) => {
                // careful, the json here is SDMX and not json from chart service
                this.setState({
                  isFetching: false,
                  type,
                  data: getLatestData(data, json, type),
                  locale,
                  messages: i18nMessages[locale],
                });
              })
              .catch((error) => this.setState({ isFetching: false, error }));
          }
        }
      })
      .catch((error) =>{
        this.setState({ isFetching: false, error })
        throw error;
      });
  }

  render = () => {
    const definedMessages = defineMessages(reduce(
      this.state.messages,
      (memo, t, id) => ({ ...memo, [id]: { id } }),
      {}
    ));
    let component;
    if (this.state.isFetching) {
      component = <FormattedMessage id="component.viewer.data.fetching" />;
    }
    else if (this.state.error) {
      component = (
        <span>
          <FormattedMessage id="component.viewer.data.error" /> {this.state.error.message}
        </span>
      );
    }
    else if (isDataEmpty(this.state.type)(this.state.data)) {
      component = <FormattedMessage id="component.viewer.data.none" />;
    }
    else {
      component = (
          <Viewer
            type={this.state.type}
            data={this.state.data}
            messages={this.state.messages}
            definedMessages={definedMessages}
          />
      );
    }

    return (
      <ThemeProvider theme={mUiTheme}>
        <IntlProvider
          locale={this.state.locale}
          key={this.state.locale}
          messages={this.state.messages}>
          <React.Fragment>
            <Helmet title={`${get(this.state.data, 'data.title.label', '...')} - OECD Chart`}>
              <link rel="icon" type="image/x-icon" href={favicon} />
            </Helmet>
            {component}
          </React.Fragment>
        </IntlProvider>
      </ThemeProvider>
    );
  }
}

// Static config
(process.env.NODE_ENV === 'development'
  ? import(`./config/${process.env.APP_ENV}.json`)
  : fetch('config.json', { credentials: 'include' })
      .then((response) => {
        if (response.ok) return response.json();
        throw new Error(response.statusText);
      })
).then((json) => {
  setTimeout(
    () => {
      const config = Object.assign({}, get(json, 'base'), get(json, process.env.TARGET_ENV));
      render(<App config={config} />, document.getElementById('root'));
    },
    process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'prototype' ? 1000 : 0
  )
}).catch((error) => {
  console.log('failed to load static config');
  render(<App />, document.getElementById('root'));
});
