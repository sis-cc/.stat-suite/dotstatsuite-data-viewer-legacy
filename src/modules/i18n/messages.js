import { addLocaleData } from 'react-intl';
import { formatTranslationMessages } from './utils';

import enLocaleData from 'react-intl/locale-data/en';
import enTranslationMessages from '../../../translations/en.json';

import frLocaleData from 'react-intl/locale-data/fr';
import frTranslationMessages from '../../../translations/fr.json';

import arLocaleData from 'react-intl/locale-data/ar';
import arTranslationMessages from '../../../translations/ar.json';

export const DEFAULT_LOCALE = 'en';
export const appLocales = ['en', 'fr', 'ar'];

addLocaleData(enLocaleData);
addLocaleData(frLocaleData);
addLocaleData(arLocaleData);

export default {
  en: formatTranslationMessages('en', enTranslationMessages),
  fr: formatTranslationMessages('fr', frTranslationMessages),
  ar: formatTranslationMessages('ar', arTranslationMessages),
};
