import { divide, get, each, isEmpty, isNil, map, omit, round, set } from 'lodash';
import * as R from 'ramda';
import ReactGA from 'react-ga';
import { rules } from '@sis-cc/dotstatsuite-components';
import { getDataFrequency } from '@sis-cc/dotstatsuite-sdmxjs'; 
import { LATEST } from './constants';

export const handleError = (response) => {
  if (response.ok) return response.json();
  throw new Error(`${response.status} ${response.statusText}`);
};

export const sendAnalytics = (gaTrackingId, shareId) => responseJson => {
  if (gaTrackingId) {
    const shareType = get(responseJson, 'type');
    const shareTitle = get(responseJson, 'data.data.title.label');
    ReactGA.event({
      category: 'Share Views',
      action: shareType,
      label: shareTitle,
      dimension1: shareId,
    });
  }
  return responseJson;
};

export const getQueryVariable = (location, key) => {
  let variable;
  each(location.search.substring(1).split('&'), (param) => {
    const [name, value] = param.split('=');
    if (name === key) return variable = value;
  });
  return variable;
}

export const getDataFromJson = (type) => (json) => {
  return {
    data: get(json, 'data.data', {}),
    options: get(json, 'data.options', {}),
    config: get(json, 'data.config', {}),
  };
};

export const getLatestData = (data, sdmxJson, type) => {
  const display = get(data, 'data.share.display');

  const customAttributes = R.pathOr({}, ['data', 'customAttributes'], data);

  const units = R.pathOr({}, ['data', 'share', 'units'], data);

  const preparedData = rules.prepareData({ data: sdmxJson }, customAttributes, units);

  const { title = {}, subtitle = {}, source = {} } = R.pipe(
    R.prop('data'),
    R.pick(['title', 'subtitle', 'source']),
  )(data);
  const titleInput = R.prop('label', title);
  const subtitleInput = R.pipe(R.head, R.prop('label'))(subtitle);
  const sourceInput = R.prop('label', source);

  const sdmxUrl = R.path(['data', 'share', 'source'], data);

  const header = rules.getHeaderProps(
    { customAttributes, data: sdmxJson, display, type, units, sdmxUrl },
    { ...preparedData, title: titleInput, subtitle: subtitleInput },
  );

  const { sourceLabel } = rules.getFooterProps(
    { data: sdmxJson, type, display, sdmxUrl },
    { sourceLabel: sourceInput },
  );

  const map = {
    topology: get(data, 'data.series', null),
    areaSelection: 'areas'
  };

  const frequency = getDataFrequency({
    attributes: R.pathOr([], ['structure', 'attributes', 'observation'], sdmxJson),
    dimensions: R.pathOr([], ['structure', 'dimensions', 'observation'], sdmxJson)
  });

  return {
    options: data.options,
    config: data.config,
    data: {
      ...header,
      frequency,
      source: R.pipe(
        R.pathOr({}, ['data', 'source']),
        R.assoc('label', sourceLabel)
      )(data),
      copyright: R.path(['data', 'copyright'], data),
      series: rules.series(
        sdmxJson,
        type,
        get(data, 'data.share.focused'),
        get(data, 'data.share.chartDimension'),
        map,
        display,
        get(data, 'data.share.formaterIds')
      ),
      share: {
        focused: rules.parseFocus(
          sdmxJson,
          type,
          get(data, 'data.share.chartDimension'),
          display,
          get(data, 'data.share.focused', {})
        )
      }
    }
  };
};

export const isDataEmpty = (type) => (data) => {
  return isEmpty(get(data, 'data'));
};

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const getYearLast2Digits = (year) => {
  if (year === 2000) {
    return '00';
  }
  return year > 2000 ? year - 2000 : year - 1900;
}

const getQuarter = (datum) => {
  const month = datum.getMonth();
  return `Q${divide(month, 3) + 1}`;
}

const getMonth = (datum) => {
  const month = datum.getMonth();
  return months[month];
}

export const appendTimelineTooltip = ({ options, data, type, config }) => {
  const frequency = data.frequency
  if (type !== 'TimelineChart' || isNil(frequency)) {
    return options;
  }
  const timeFormatter = datum => {
    const date = new Date(datum);
    if (frequency === 'quarterly') {
      return `${getQuarter(date)} ${date.getFullYear()}`;
    }
    if (frequency === 'monthly') {
      return `${getMonth(date)} ${getYearLast2Digits(date.getFullYear())}`
    }
    return date.getFullYear();
  }
  const tooltipFonts = get(config, 'fonts.chart.tooltip', {});
  const primaryFontFamily = get(tooltipFonts, 'primary.fontFamily', 'bernino-sans-narrow-regular');
  const primaryFontSize = get(tooltipFonts, 'primary.fontSize', 12);
  const secondaryFontFamily = get(tooltipFonts, 'secondary.fontFamily', 'bernino-sans-bold');
  const secondaryFontSize = get(tooltipFonts, 'secondary.fontSize', 16);
  const secondaryFonts = `font-family: '${secondaryFontFamily}'; font-size: ${secondaryFontSize}px;`
  const primaryFonts = (color) => (`
    font-size: ${primaryFontSize}px;
    font-family: '${primaryFontFamily}';
    color: ${color};
    padding: 5px;
    border: 1px solid ${color};
    background: white;
    opacity: .8;
  `);

  return ({
    ...options,
    serie: {
      ...options.serie,
      tooltip: {
        layout: (serie, datum, color) => (`
          <div style="${primaryFonts(color)}">
            <div style="text-align: right;">
              ${map(get(datum, 'dimensionValues', {}), 'name').join('<br />')}
            </div>
            <div style="text-align: right;">
              ${timeFormatter(datum.x)}
            </div>
            <div style="text-align: right; ${secondaryFonts}">
              ${datum.formatedValue || round(datum.y, 2)}
            </div>
          </div>
        `)
      }
    }
  });
}

